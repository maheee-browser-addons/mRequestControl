if (!window.lib) window.lib = {};


lib.options = {};

lib.str2ab = (str) => {
  return new TextEncoder().encode(str);
};

lib.findRule = (options, method, url) => {
  for (let ruleId of Object.keys(options.rules)) {
    let rule = options.rules[ruleId];
    if (rule.enabled) {
      if (rule.method == method) {
        if (url.match(new RegExp(rule.pattern))) {
          return rule;
        }
      }
    }
  }
};


browser.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (!message || !message.id) {
    return;
  }
  if (message.id == 'options') {
    lib.options[message.tabId] = message.data;
  } else if (message.id == 'request_options') {
    sendResponse(lib.options[message.tabId]);
  }
});


browser.webRequest.onBeforeRequest.addListener((request) => {
  if (!lib.options[request.tabId]) {
    return;
  }

  let options = lib.options[request.tabId];
  if (!options.enableEditing) {
    return;
  }

  let rule = lib.findRule(options, request.method, request.url);
  if (!rule) {
    return;
  }

  let filter = browser.webRequest.filterResponseData(request.requestId);

  filter.ondata = (event) => {
    filter.write(lib.str2ab(rule.response));
    filter.close();
  };
}, {urls: ['<all_urls>']}, ['blocking']);
