if (!window.lib) window.lib = {};


/*
 * Data
 */
lib.options = {
  enableRecording: false,
  enableEditing: true,
  rules: {}
};
lib.distOptions = () => {
  browser.runtime.sendMessage({
    id: 'options',
    tabId: browser.devtools.inspectedWindow.tabId,
    data: lib.options
  });
};


/*
 * Initialize Elements
 */
lib.elementList = [
  'button_record', 'button_clear', 'button_enable',
  'tab_requests', 'tab_rules',
  'table_requests', 'table_body_requests',
  'table_rules', 'table_body_rules',
  'form', 'form_input_id', 'form_input_method', 'form_input_pattern', /*'form_input_code', */'form_input_response', 'form_input_enabled',
  'button_form_reset', 'button_form_submit', 'button_form_delete'
]
lib.elements = {};
for (let e of lib.elementList) {
  lib.elements[e] = document.getElementById(e);
}


/*
 * Input Element Functions
 */
lib.switchTab = (activeId) => {
  lib.elements['tab_requests'].classList.remove('active');
  lib.elements['tab_rules'].classList.remove('active');
  lib.elements['table_requests'].classList.add('hidden');
  lib.elements['table_rules'].classList.add('hidden');

  lib.elements['tab_' + activeId].classList.add('active');
  lib.elements['table_' + activeId].classList.remove('hidden');
};

lib.setFormDisabled = (disabled) => {
  lib.elements['form_input_method'].disabled = disabled;
  lib.elements['form_input_pattern'].disabled = disabled;
  //lib.elements['form_input_code'].disabled = disabled;
  lib.elements['form_input_response'].disabled = disabled;
  lib.elements['form_input_enabled'].disabled = disabled;
  lib.elements['button_form_reset'].disabled = disabled;
  lib.elements['button_form_submit'].disabled = disabled;
  lib.elements['button_form_delete'].disabled = disabled;
};

lib.clearForm = () => {
  lib.setFormValues('', 'GET', '', '0', '', false);
};

lib.setSelectValue = (element, value) => {
  let i = 0;
  for (let option of element.options) {
    option.removeAttribute('selected');
    if (option.value == value) {
      element.options.selectedIndex = i;
      option.setAttribute('selected', 'selected');
    }
    ++i;
  }
};

lib.setFormValues = (id, method, pattern, code, response, enabled) => {
  lib.elements['form_input_id'].setAttribute('value', id);
  lib.setSelectValue(lib.elements['form_input_method'], method);
  lib.elements['form_input_pattern'].setAttribute('value', pattern);
  //lib.setSelectValue(lib.elements['form_input_code'], code);
  lib.elements['form_input_response'].innerHTML = response;
  if (enabled) {
    lib.elements['form_input_enabled'].setAttribute('checked', 'checked');
  } else {
    lib.elements['form_input_enabled'].removeAttribute('checked');
  }
  lib.elements['form'].reset();
};

lib.setFormValuesFromRule = (rule) => {
  lib.setFormValues(rule.id,
    rule.method, rule.pattern,
    rule.code, rule.response, rule.enabled);
};

lib.removeChildren = (element) => {
  while (element.lastChild) {
      element.removeChild(element.lastChild);
  }
};

lib.getFormValues = (form) => {
  let values = {};
  for (let i = 0; i < form.elements.length; ++i) {
    let e = form.elements[i];
    if (e.name) {
      if (e.type == 'checkbox') {
        values[e.name] = e.checked;
      } else {
        values[e.name] = e.value;
      }
    }
  }
  return values;
};


/*
 * Event Handler
 */
lib.elements['tab_requests'].addEventListener('click', () => { lib.switchTab('requests'); });
lib.elements['tab_rules'].addEventListener('click', () => { lib.switchTab('rules'); });

lib.elements['button_record'].addEventListener('click', () => {
  lib.elements['button_record'].classList.toggle('active')
  lib.options.enableRecording = !lib.options.enableRecording;
  lib.distOptions();
});
lib.elements['button_enable'].addEventListener('click', () => {
  lib.elements['button_enable'].classList.toggle('active')
  lib.options.enableEditing = !lib.options.enableEditing;
  lib.distOptions();
});
lib.elements['button_clear'].addEventListener('click', () => {
  lib.removeChildren(lib.elements['table_body_requests']);
  lib.switchTab('requests');
});

lib.elements['button_form_delete'].addEventListener('click', () => {
  let values = lib.getFormValues(lib.elements['form']);
  if (values && values.id) {
    delete lib.options.rules[values.id];
    lib.updateRules();
    lib.distOptions();
    lib.clearForm();
    lib.setFormDisabled(true);
  }
});
lib.elements['form'].addEventListener('submit', (event) => {
  event.preventDefault();

  let values = lib.getFormValues(event.target);
  lib.options.rules[values.id] = values;
  lib.setFormDisabled(true);
  setTimeout(() => {
    lib.setFormValuesFromRule(values);
    lib.updateRules();
    lib.switchTab('rules');
    lib.distOptions();
    lib.setFormDisabled(false);
  }, 100);

  return false;
});


/*
 * Init
 */
lib.setFormDisabled(true);
lib.switchTab('requests');


/*
 * Adding Lines
 */
lib.addRequestLine = (method, url, type, size, clickHandler) => {
  let line = document.createElement("tr");
  line.innerHTML =
    '<td>' + method + '</td>' +
    '<td>' + url + '</td>' +
    '<td>' + type + '</td>' +
    '<td>' + size + '</td>';
  line.addEventListener('click', clickHandler);
  lib.elements['table_body_requests'].appendChild(line);
};

lib.addRequestLineFromEvent = (event) => {
  let contentTypeHeader = event.response.headers.find(el => el.name == 'Content-Type');

  let clickHandler = () => {
    lib.clearForm();
    lib.setFormDisabled(true);
    event.getContent().then(content => {
      lib.setFormValues((+ new Date()),
        event.request.method,
        '^' + event.request.url.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') + '$',
        '0', content[0], true);
      lib.setFormDisabled(false);
    });
  };

  lib.addRequestLine(
    event.request.method, event.request.url,
    (contentTypeHeader ? contentTypeHeader.value : '?'),
    (event.response.content.size || '?'),
    clickHandler);
};


lib.addRuleLine = (method, pattern, active, clickHandler) => {
  let line = document.createElement("tr");
  line.innerHTML =
    '<td>' + method + '</td>' +
    '<td>' + pattern + '</td>' +
    '<td>' + active + '</td>';
  line.addEventListener('click', clickHandler);
  lib.elements['table_body_rules'].appendChild(line);
};

lib.addRule = (rule) => {
  lib.addRuleLine(rule.method, rule.pattern, rule.enabled, () => {
    lib.clearForm();
    lib.setFormDisabled(true);
    lib.setFormValuesFromRule(rule);
    lib.setFormDisabled(false);
  });
};

lib.updateRules = () => {
  lib.removeChildren(lib.elements['table_body_rules']);

  for (let ruleId of Object.keys(lib.options.rules)) {
    let rule = lib.options.rules[ruleId];
    lib.addRule(rule);
  }
};


/*
 * Internal handling
 */
if (window['browser']) {
  browser.runtime.sendMessage({
    id: 'request_options',
    tabId: browser.devtools.inspectedWindow.tabId
  }).then(options => {
    if (options) {
      lib.options.rules = options.rules;
      lib.updateRules();
      lib.distOptions();
    }
  });
  browser.devtools.network.onRequestFinished.addListener((event) => {
    if (!lib.options.enableRecording) {
      return;
    }
    lib.addRequestLineFromEvent(event);
  });
}
