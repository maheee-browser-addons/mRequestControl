# ![](./plugin/icons/default-48.png) RequestControl

## Description

mRequestControl is a Firefox Plugin which lets you manipulate the responses of HTTP requests.

## Features

Set up URL patterns and define a different response. The request will still be made but the page gets the changed response in return.

## How to use

1. Open Developer Tools (F12) on page you want to work at
2. Select "RequestControl" tab
3. Enabling recording (circle icon) and reload the page
4. Select request you want to modify, change pattern if necessary (it's regex) and save as rule
5. Disable recording (not necessary), enable modifying (triangle icon; enabled by default)
6. Reload page and changes should get applied

## Screenshots

![](./res/screenshot_1.png)

![](./res/screenshot_2.png)
